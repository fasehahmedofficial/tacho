<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit81cbe65b8442c51dce292c8c04ec9017
{
    public static $prefixLengthsPsr4 = array (
        'F' => 
        array (
            'Faseh\\Tacho\\' => 12,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Faseh\\Tacho\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit81cbe65b8442c51dce292c8c04ec9017::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit81cbe65b8442c51dce292c8c04ec9017::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit81cbe65b8442c51dce292c8c04ec9017::$classMap;

        }, null, ClassLoader::class);
    }
}
