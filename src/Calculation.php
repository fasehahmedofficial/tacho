<?php
namespace Faseh\Tacho;
class Calculation{
    public static function sum(array $parms){
        if(!is_array($parms)){
            return ['error' => 'Must be an Array'];
        }
        return array_sum($parms);
    }
    public static function product(array $parms){
        if(!is_array($parms)){
            return ['error' => 'Must be an Array'];
        }
        $response = 0;
        foreach($parms as $key=>$value){
            $response *= $value;
            
        }
        return $response;
    }
    
}
?>